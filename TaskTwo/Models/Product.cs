﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskTwo.Models
{
    public class Product
    { 
        public int Id {get;set;}
        [Required(ErrorMessage = "Product name is required")]
        public string ProductName { get; set; }
        [Required(ErrorMessage = "Product price is required")]
        public int Price { get; set; }
        [Required(ErrorMessage = "Product describtion is required")]
        public string Describe { get; set; }
        public DateTime DateUpd { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public Product()
        {
            Orders = new List<Order>();
            
        }
    }
}