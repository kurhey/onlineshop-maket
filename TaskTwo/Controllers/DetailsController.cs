﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskTwo.Models;

namespace TaskTwo.Controllers
{
    public class DetailsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            using (PurchaseContext db = new PurchaseContext())
            {
                var users = db.Users.ToList();
                return View(users);
            }
        }

        public ActionResult Products()
        {
            using (PurchaseContext db = new PurchaseContext())
            {
                var products = db.Products.ToList();
                return View(products);
            }
        }

        public ActionResult Orders()
        {
            using (PurchaseContext db = new PurchaseContext())
            {
                var orders = db.Orders.ToList();
                return View(orders);
            }
        }

        public ActionResult EditingUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateUser(User user)
        {
            
            return View("Index");
        }
    }
}
